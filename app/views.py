from flask import render_template, flash, redirect, url_for, session
from sqlalchemy.exc import IntegrityError
from app import app, db, models, admin
from functools import wraps
from flask import g, request, redirect, url_for
from .forms import RegisterForm, SignInForm, SetForm, RoutineForm, PasswordForm, DefaultRoutineForm
from .models import User, Exercise, Sets, Routine
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import date
from collections import Counter
from passlib.hash import sha256_crypt

@app.route('/')
def index():
    return render_template("landingPage.html", title="GymTracker")

@app.route('/home') #A user's homepage
def home():
    user = models.User.query.filter_by(username=session['username']).first()
    routines=user.routines
    return render_template("account.html", title="Home", user=user, routines=routines)

@app.route('/signin', methods=['GET', 'POST'])
def signin():
    form = SignInForm()
    if form.validate_on_submit(): 
        username = request.form.get('username')
        password = request.form.get('password')
        user = models.User.query.filter_by(username=username).first()
        if user == None: #check for user nonetype
            flash('Incorrect username')
            return redirect(url_for('signin'))
        else:
            if sha256_crypt.verify(form.password.data, user.password): #check hash
                session['username'] = username 
                session['loggedin'] = True
                flash('Signed in')
                return redirect(url_for('home'))
            else:
                flash("Password is incorrect")

    return render_template("signin.html", title="Sign In", form=form)

@app.route('/logout')
def logout():
    session.pop('loggedin', False) #Remove vairables from session
    session.pop('username', None)
    flash('You have successfully logged out')
    return redirect(url_for('index'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        hashed_password = sha256_crypt.encrypt(form.password.data) #Hash password
        user = models.User(username=form.username.data, password=hashed_password, email=form.email.data)
        db.session.add(user)
        try:
            db.session.commit()
            flash('Thank you for registering')
            return redirect(url_for('signin'))
        except IntegrityError: #Breaches unique constraint
            flash('There is already a user registered with this email/username')
            db.session.rollback()
    return render_template("register.html", title="Register", form=form)

@app.route('/settings', methods=['GET', 'POST'])
def settings():
    user = models.User.query.filter_by(username=session['username']).first()
    form = PasswordForm()
    if form.validate_on_submit():
        if sha256_crypt.verify(form.oldPassword.data, user.password): #check hash
            new_hashed_password = sha256_crypt.encrypt(form.newPassword.data) #Hash password
            user.password = new_hashed_password
            db.session.add(user)
            db.session.commit()
            flash('Successfully changed password')
        else:
            flash('Incorrect password')
    return render_template("settings.html", title="Account Settings", form=form, user=user)

@app.route('/createSet/<routine_id>', methods=['GET', 'POST'])
def createSet(routine_id):
    user = models.User.query.filter_by(username=session['username']).first() 

    #change into choices formatting ("name", "name")
    exercises = [ (e.name, e.name) for e in models.Exercise.query.all() ]
    form = SetForm()
    form.exercise.choices = exercises

    if form.validate_on_submit():
        exercise = models.Exercise.query.filter_by(name=form.exercise.data).first()
        if exercise != None: 
            exercise_id = exercise.id
            routine = models.Routine.query.filter_by(id=routine_id).first()#find routine to add set to
            set = models.Sets(reps=form.reps.data, weight=form.weight.data, exercise_id=exercise_id)# create set object
            set.exercise.append(exercise)
            routine.sets.append(set)
            db.session.add(set, routine)
            db.session.commit()
            flash('Successfully added set')
            return redirect(url_for('viewRoutines'))
        else:
            flash('Exercise does not exist')

    return render_template("createSets.html", title="Create Set", user=user, form=form)

@app.route('/viewRoutines')
def viewRoutines():
    user = models.User.query.filter_by(username=session['username']).first()
    routines = user.routines
    return render_template("viewRoutines.html", title="View Routines", user=user, routines=routines)

@app.route('/createRoutine', methods=['GET', 'POST'])
def createRoutine():
    form = RoutineForm() 
    user = models.User.query.filter_by(username=session['username']).first()
    if form.validate_on_submit():
        routine = models.Routine(name=form.name.data, public=0) 
        user.routines.append(routine)
        db.session.add(routine, user)
        db.session.commit()
        flash('Successfully added routine') 
        return redirect(url_for('viewRoutines'))
    return render_template("createRoutine.html", title="Create Routine", form=form)

@app.route('/defaultRoutine', methods=['GET', 'POST'])
def defaultRoutine():
    user = models.User.query.filter_by(username=session['username']).first()
    #put routines in format for choices ('routine', 'routine')
    routines = [ (r.name, r.name) for r in models.Routine.query.filter(Routine.public==1) ]
    form = DefaultRoutineForm()
    form.name.choices = routines

    if form.validate_on_submit():
        routine = models.Routine.query.filter_by(name=form.name.data).first()
        user.routines.append(routine)
        db.session.add(user, routine)
        db.session.commit()
        flash('Successfully added routine')
        return redirect(url_for('home'))
    return render_template("defaultRoutine.html", title="Default Routines", form=form)

@app.route('/nearestGym')
def nearestGym():
    return render_template("nearestGym.html", title="Nearest Gym")

@app.route('/viewData')
def viewData():
    user = models.User.query.filter_by(username=session['username']).first()
    routines = user.routines
    data = []
    for r in routines:
        for s in r.sets: #loop through each set in each routine
            exercise = s.exercise
            muscle = exercise.muscle
            data.append(muscle)
    data = Counter(data)
    #collate each instance a muscle group is used per routine?
    temp = "".join([ '"' + i[0] + '", ' for i in data.most_common() ])
    return render_template("viewData.html", title="View Data", user=user, data=data, temp=temp)
