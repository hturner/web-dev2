from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_admin import Admin

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

app.secret_key = "randomstring"

migrate = Migrate(app, db)
admin = Admin(app,template_mode='bootstrap3')

login_manager = LoginManager()
login_manager.init_app(app)

if __name__ == '__main__':
    app.run()

from app import views, models
