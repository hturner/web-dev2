from app import db

user_routine = db.Table('User Routine',
db.Column('user_id', db.Integer, db.ForeignKey("user.id")),
db.Column('routine_id', db.Integer, db.ForeignKey("routine.id"))
)

class Exercise(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(500), unique=True)
    muscle = db.Column(db.String(100))
    def __repr__(self):
        return '' % (self.id, self.name, self.muscle)

class Sets(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    routine_id = db.Column(db.Integer, db.ForeignKey('routine.id'))
    reps = db.Column(db.Integer)
    weight = db.Column(db.Integer)
    exercise_id = db.Column(db.Integer, db.ForeignKey('exercise.id'))
    exercise = db.relationship(Exercise, uselist=False)
    def __repr__(self):
        return '' % (self.id, self.reps, self.weight)

class Routine(db.Model):#Should be made up of several sets and muscles
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30))
    sets = db.relationship('Sets', backref=db.backref('sets', uselist=False), lazy='dynamic')
    public = db.Column(db.Boolean)
    #user_id = db.Column(db.Integer, db.ForeignKey('user.id')) 
    def __repr__(self):
        return '' % (self.user_id, self.sets, self.date)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(500), unique=True)
    username = db.Column(db.String(20), unique=True)
    password = db.Column(db.String(500))
    routines = db.relationship('Routine', secondary=user_routine, lazy='subquery', backref=db.backref('routines', lazy=True))
    def __repr__(self):
        return '' % (self.id, self.email, self.username, self.password, self.routines)
