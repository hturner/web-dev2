from flask_wtf import Form
from wtforms import IntegerField, StringField, DateField, TextAreaField, PasswordField, SelectField
from wtforms.validators import DataRequired, Length, Optional, Email, EqualTo, InputRequired
from datetime import date

class ExerciseForm(Form):
    name = StringField('Name', validators=[DataRequired()])
    muscles = StringField('Muscles', validators=[DataRequired()])

class RoutineForm(Form):
    name = StringField('Name', validators=[DataRequired(), Length(min=1, max=20)])

class SignInForm(Form):
    username = StringField('Username', validators=[DataRequired()]) 
    password = PasswordField('Password', validators=[DataRequired()]) 

class RegisterForm(Form):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('New Password', validators=[DataRequired(), EqualTo('confirm', message='Passwords must match')])
    confirm = PasswordField('Repeat Password')

class PasswordForm(Form):
    oldPassword = PasswordField('Old Password', validators=[DataRequired()])
    newPassword = PasswordField('New Password', validators=[DataRequired(), EqualTo('confirm', message='Passwords must match')])
    confirm = PasswordField('Repeat Password')

class SetForm(Form):
    exercise = SelectField('Exercise',choices=[], validators=[DataRequired()])
    weight = IntegerField('Weight', validators=[DataRequired()])
    reps = IntegerField('Reps', validators=[DataRequired()])

class DefaultRoutineForm(Form):
    name = SelectField('Name', choices=[], validators=[DataRequired()])
